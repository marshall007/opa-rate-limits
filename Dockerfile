FROM golang:1.18-alpine as build

WORKDIR /app
COPY ./server .
RUN go build .

COPY ./bundle /bundle

CMD [ "/app/opa-rate-limits", "run", "-b", "/bundle", "--server" ]
