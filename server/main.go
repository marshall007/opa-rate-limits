package main

import (
	"fmt"
	"hash/crc32"
	"os"
	"time"

	"github.com/cespare/xxhash/v2"
	"github.com/go-redis/redis/v9"
	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/cmd"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/types"
)

func main() {
	client := redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_HOST"),
	})

	rego.RegisterBuiltin2(
		&rego.Function{
			Name:    "redis.count",
			Decl:    types.NewFunction(types.Args(types.S, types.S), types.A),
			Memoize: false,
		},
		redisIncrExp(client),
	)

	rego.RegisterBuiltin1(
		&rego.Function{
			Name:    "hash.xxhash",
			Decl:    types.NewFunction(types.Args(types.S), types.N),
			Memoize: true,
		},
		hashXXH(),
	)

	rego.RegisterBuiltin1(
		&rego.Function{
			Name:    "hash.crc32",
			Decl:    types.NewFunction(types.Args(types.S), types.N),
			Memoize: true,
		},
		hashCRC32(),
	)

	if err := cmd.RootCommand.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func hashXXH() rego.Builtin1 {
	return func(bctx rego.BuiltinContext, a *ast.Term) (*ast.Term, error) {
		var data string

		if err := ast.As(a.Value, &data); err != nil {
			return nil, err
		}

		return ast.UIntNumberTerm(xxhash.Sum64String(data)), nil
	}
}

func hashCRC32() rego.Builtin1 {
	return func(bctx rego.BuiltinContext, a *ast.Term) (*ast.Term, error) {
		var data string

		if err := ast.As(a.Value, &data); err != nil {
			return nil, err
		}

		return ast.UIntNumberTerm(uint64(crc32.ChecksumIEEE([]byte(data)))), nil
	}
}

func redisIncrExp(client *redis.Client) rego.Builtin2 {
	return func(bctx rego.BuiltinContext, a, b *ast.Term) (*ast.Term, error) {
		var key, duration_s string

		if err := ast.As(a.Value, &key); err != nil {
			return nil, err
		} else if ast.As(b.Value, &duration_s); err != nil {
			return nil, err
		}

		duration, err := time.ParseDuration(duration_s)
		if err != nil {
			return nil, err
		}

		var incr *redis.IntCmd
		var ttl *redis.DurationCmd

		if _, err = client.TxPipelined(bctx.Context, func(pipe redis.Pipeliner) error {
			incr = pipe.Incr(bctx.Context, key)
			pipe.ExpireNX(bctx.Context, key, duration)
			ttl = pipe.TTL(bctx.Context, key)

			return nil
		}); err != nil {
			return nil, err
		}

		return ast.ArrayTerm(
			ast.IntNumberTerm(int(incr.Val())),
			ast.IntNumberTerm(int(ttl.Val().Nanoseconds())),
		), nil
	}
}
