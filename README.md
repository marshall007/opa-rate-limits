```sh
$ docker-compose up --build
```

```sh
$ cat test/input.json
{
  "input": {
    "user": "marshall007",
    "action": "issues.create",
    "namespace_id": 1
  }
}
```

### Rate Limit Policy

```sh
$ curl "http://localhost:8181/v1/data/limits?metrics=true" -X POST -H "Content-Type: application/json" -d @test/input.json
{
  "metrics": {
    "counter_server_query_cache_hit": 1,
    "timer_rego_external_resolve_ns": 353,
    "timer_rego_input_parse_ns": 45808,
    "timer_rego_query_eval_ns": 390515,
    "timer_server_handler_ns": 459147
  },
  "result": {
    "allow": true,
    "limit": {
      "count": 1,
      "key": "issues.create:user:marshall007",
      "threshold": 5,
      "ttl_ns": 60000000000
    }
  }
}
```

### Feature Flag Configuration

```sh
$ curl "http://localhost:8181/v1/data/features?metrics=true" -X POST -H "Content-Type: application/json" -d @test/input.json
{
  "metrics": {
    "counter_server_query_cache_hit": 1,
    "timer_rego_external_resolve_ns": 690,
    "timer_rego_input_parse_ns": 49563,
    "timer_rego_query_eval_ns": 269962,
    "timer_server_handler_ns": 348282
  },
  "result": {
    "features": {
      "dark_mode": true,
      "fun_feature": false,
      "group_level_changes": true
    }
  }
}
```

### Patch Feature Flag Rollout

This would set the rollout percentage of `group_level_changes` to 0.

```sh
$ cat test/input.json
[
  {
    "op": "replace",
    "path": "/group_level_changes/rollout",
    "value": 0
  }
]
```

```sh
$ curl "http://localhost:8181/v1/data/feature_flags" -X PATCH -H "Content-Type: application/json-patch+json" -d @test/patch-flag.json
```
