package features

import data.feature_flags

features[flag] := enabled {
  config := data.feature_flags[flag]
  config.active == true
  key := concat(":", [ flag, config.scope, input[config.scope] ])
  enabled := (hash.crc32(key) % 100) < config.rollout
}
