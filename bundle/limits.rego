package limits

import data.rate_limits

default allow = false

allow {
  limit.count < limit.threshold
}

limit := result {
  config := data.rate_limits[input.action]
  key := concat(":", [ input.action, config.scope, input[config.scope] ])
  count := redis.count(key, config.interval)

  result := {
    "key": key,
    "count": count[0],
    "threshold": config.threshold,
    "ttl_ns": count[1]
  }
}
